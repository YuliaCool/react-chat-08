import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles.css';

export class ChatHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chatName: "My chat"
        }
    }
    
    render() {
        const participantCount = this.props.participantCount;
        const messageCount = this.props.messageCount;
        const timeLastMessage = this.props.timeLastMessage;

        return (
            <div className="header-container">
                <div className="left-part">
                    <div>{this.state.chatName}</div>
                    <div>
                        <span>{participantCount}</span>
                        <span> participants</span>
                    </div>
                    <div>
                        <span>{messageCount}</span>
                        <span> messages</span>
                    </div>
                </div>
                <div className="right-part">
                    <div>
                        <span>last message at </span>
                        <span>{timeLastMessage}</span>
                    </div>   
                </div>
            </div>
        );
    }
};
ChatHeader.propTypes = {
    participantCount: PropTypes.number,
    messageCount: PropTypes.number,
    timeLastMessage: PropTypes.string
}
export default ChatHeader;