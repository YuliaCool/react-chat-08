import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles.css';

export class PageHeader extends Component {

    render() {
        return (
            <div className="header">
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Telegram_logo.svg/512px-Telegram_logo.svg.png" title="logo" alt="logo" />
            </div>
        );
    }
};

export default PageHeader;