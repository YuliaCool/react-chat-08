import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles.css';

export class Message extends Component {
    constructor(props) {
        super(props);
    }

    getDeletedMessageId = () => {
        return this.props.onDelete(this.props.message.id);
    }

    getEditedMessageId = (messageText) => {
        return this.props.onEdit(this.props.message.id, messageText);
    }
    getLikedMessageId = () => {
        //this.setState({ isLiked: true});
        return this.props.onLike(this.props.message.id);
    }

    render() { 
        const message = this.props.message;
        const isMine = message.userId === this.props.currentUserId;
        const isLiked = this.props.message.isLiked;
        return (
            <div className={`message-container ${isMine ? "my" : ""}`}>
                <div className="message-header">
                    { isMine 
                        ?
                        <div>
                            <button className={'message-controls'} onClick={this.getEditedMessageId}>edit</button>
                            <button className={'message-controls'} onClick={this.getDeletedMessageId}>delete</button>
                        </div>
                        : null
                    }
                </div>
                <div className={"messageBody"}>
                    { !isMine 
                        ?  <div className="avatar">
                                <img src={ message.avatar }/>
                            </div>
                        : null
                    }               
                    <div className="message-text">
                        <p>{message.text}</p>
                    </div>
                </div>
                
                <div className="message-footer">
                    <div>
                         <span>{message.countLikes}</span>
                        <span> likes </span>
                    </div>
                    { !isMine 
                        ? <button 
                                className={"message-controls"} 
                                //onClick={`${isLiked ? this.getLikedMessageId : null}`}> 
                                onClick={this.getLikedMessageId}> 
                                    {isLiked ? 'I like it!' : 'like' }
                          </button> 
                        : null
                    }
                </div>
            </div>
        )
    }
};

Message.propTypes = {
    message: PropTypes.object,
    currentUserId: PropTypes.string,
    onEdit: PropTypes.func,
    onDelete: PropTypes.func,
    onLike: PropTypes.func
};

export default Message;