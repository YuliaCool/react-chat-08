import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles.css';

export class EditMessage extends Component {

    constructor(props){
        super(props);
        this.state = {
           textBody: undefined
        };
    }

    updateTextArea(ev) {
        return this.setState({ textBody: ev.target.value })
    };

    editMessage = async () => {
       this.props.editMessageHandler(this.state.textBody);
    };
    
    render(){
        const defaultValue = this.state.textBody === undefined
                              ? this.props.textBody
                              : this.state.textBody;
        return (
            <div className="add-message-container">
                    <textarea className="message-input"
                        value={defaultValue}
                        placeholder="Message"
                        onChange={ev => this.updateTextArea(ev)}
                    >
                    </textarea>
                <button type="submit" onClick={this.editMessage}> Update</button>
            </div> 
        );
    }
};

EditMessage.propTypes = {
    textBody: PropTypes.string,
    editMessageHandler: PropTypes.func
};

export default EditMessage;