import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles.css';
import Messsage from '../Message/Message';

export class MessageList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const messages = this.props.messages;
        const currentUserId = this.props.currentUserId;
        const onDelete = this.props.onDelete; 
        const onEdit = this.props.onEdit;
        const onLike = this.props.onLike;
        return (
            <div className="message-list-container">
                { messages.map( message => (
                    <Messsage
                        currentUserId={currentUserId}
                        message={message}
                        key={message.id}
                        onDelete={onDelete} // Maybe it better to use Context here? But level of hierarchy is low  
                        onEdit={onEdit}     // and can we add to Context function? 
                        onLike={onLike}
                    />
                ))}
            </div>
        );
    }
};

MessageList.propTypes = {
    messages: PropTypes.array,
    currentUserId: PropTypes.string, 
    onDelete: PropTypes.func,
    onEdit: PropTypes.func,
    onLike: PropTypes.func
}

export default MessageList;