import React from 'react';
import logo from './logo.svg';
import './App.css';
import Chat from './containers/Chat';


function App() {
  return (
    <div className="App">
      <header >
      </header>
      <div className="App-content">
        <Chat />
      </div>
    </div>
  );
}

export default App;
