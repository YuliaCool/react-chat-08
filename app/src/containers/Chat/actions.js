import * as messageService from '../../services/messageService';


export const loadMessages = async () => {
    const allMessages = await messageService.getAllMessages();
    return allMessages;
}

export const countParticipants = (allMessages) => {
    return messageService.getCountParticipants(allMessages);
}

export const countMessages = allMessages => {
    return allMessages.length;
}

export const authorizateUser = (user) => {
    sessionStorage.setItem("username", user.id);
}

export const findLastMessage = (messages) => {
    const mostRecentDate = new Date(Math.max.apply(null, messages.map( messageItem => {
        return new Date(messageItem.createdAt);
     })));
     const mostRecentMessage = messages.filter( messageItem => { 
         const date = new Date( messageItem.createdAt ); 
         return date.getTime() == mostRecentDate.getTime();
     })[0];
     return mostRecentMessage;
}