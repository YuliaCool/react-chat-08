import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PageHeader from '../../components/PageHeader/PageHeader';
import PageFooter from '../../components/PageFooter/PageFooter';
import ChatHeader from '../../components/ChatHeader/ChatHeader';
import MessageList from '../../components/MessageList/MessageList';
import AddMessage from '../../components/AddMessage';
import EditMessage from '../../components/EditMessage';
import { v4 as uuidv4 } from 'uuid';

import * as actions from './actions';
import './styles.css';

class Chat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            allMessages: undefined, 
            participantsCount: 0,
            messagesCount: 0,
            messagesLast: undefined,
            isEditing: false,
            editingMessageId: undefined,
            likesMap: undefined,

            // when this App will have login and authentification functionality, 
            // get user information from other component and set to this.state, add to sessionStorage
            currentUser: {
                id: "533b5230-1b8f-11e8-9629-c7eca82aa7bd",
                name: "Wendy",
                avatar: "https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng"
            },
        }
        this.addMessage = this.addMessage.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);
        this.editMessage = this.editMessage.bind(this);
        this.editMessageHandler = this.editMessageHandler.bind(this);
        this.likeMessage = this.likeMessage.bind(this);
    }

    componentDidMount(){
        //actions.authorizateUser(this.state.currentUser);
        actions.loadMessages()
        .then((allMessagesResponce) => {
            allMessagesResponce.map(message => {
                    message.countLikes = 0;
                    message.createdAt = new Date (message.createdAt);
                }
            );
            this.setState({ 
                isLoading: false,
                allMessages: allMessagesResponce,
                participantsCount: actions.countParticipants(allMessagesResponce),
                messagesCount: actions.countMessages(allMessagesResponce),
                messagesLast: actions.findLastMessage(allMessagesResponce).createdAt.toLocaleTimeString(),
                likesMap: new Map(),
                isLiked: false
            });

        })
        .then(() => { console.log(this.state.allMessages) });
    }

    addMessage(messageText) {
        const now = new Date();
        const newMessage = {
            id: uuidv4(),
            text: messageText,
            createdAt: now.toISOString(),
            editedAt: "",
            userId: this.state.currentUser.id,
            user: this.state.currentUser.name,
            avatar: this.state.currentUser.avatar
        }
        
        this.state.allMessages.push(newMessage);

        this.setState({
            messagesCount: ++this.state.messagesCount,
            messagesLast: now.toLocaleTimeString()
        }); 
    }
    deleteMessage(id) {
        this.state.allMessages = this.state.allMessages.filter(message => message.id != id);
        this.setState({
            messagesCount: --this.state.messagesCount
        });
    }
    editMessage(id) {
        this.setState({
            isEditing: true,
            editingMessageId: id
        })
    }
    editMessageHandler(updatedText) {
        const updatedMessageIndex = this.state.allMessages.findIndex(message => message.id === this.state.editingMessageId);
        const updatedMessage = this.state.allMessages.find(message => message.id === this.state.editingMessageId);
        updatedMessage.text = updatedText;
        updatedMessage.editedAt = new Date().toISOString();
        this.state.allMessages[updatedMessageIndex] = updatedMessage;

        this.setState({
            isEditing: false,
            editingMessageId: ""
        })
    }

    likeMessage(id) {
        const likedMessageIndex = this.state.allMessages.findIndex(message => message.id === id);
        const likedMessage = this.state.allMessages.find(message => message.id === id);
        let previousCount = this.state.allMessages[likedMessageIndex].countLikes;
        likedMessage.countLikes = ++previousCount;
        console.log(previousCount);
        this.state.allMessages[likedMessageIndex].isLiked = true;
        this.state.allMessages[likedMessageIndex].countLikes = likedMessage.countLikes;

        this.state.likesMap.set(id, this.state.currentUser);
    }

    render() {
        const editingMessageText = this.state.isEditing 
            ? this.state.allMessages.find(message => message.id === this.state.editingMessageId).text
            : null;
        return (
            <div>
                <div>
                    <PageHeader />
                </div>
                {   this.state.isLoading 
                    ?  <div className="loader-container">
                        <div className="loader">
                            <span id="one"></span>
                            <span id="two"></span>
                            <span id="three"></span>
                            <span id="four"></span>
                        </div>
                        </div>
                    : <div className="chat-container">
                        <ChatHeader 
                            participantCount={this.state.participantsCount} 
                            messageCount={this.state.messagesCount} 
                            timeLastMessage={this.state.messagesLast} //TO DO
                        />
                        <MessageList 
                            messages={this.state.allMessages} 
                            currentUserId={this.state.currentUser.id}
                            onDelete={this.deleteMessage} 
                            onEdit={this.editMessage}
                            onLike={this.likeMessage}
                        />
                        { this.state.isEditing 
                            ? <EditMessage editMessageHandler = { this.editMessageHandler } textBody={editingMessageText}/>
                            : <AddMessage addMessageHandler = { this.addMessage } />
                        }
                        
                    </div>
                }
               
                <div>
                    <PageFooter />
                </div>
            </div>
        );
    }
};

Chat.propTypes = {
    //???
}

export default Chat;